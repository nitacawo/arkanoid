﻿package systems
{
	import flash.display.Sprite;
	import io.nfg.secs.EntityList;
	import components.*;
	import io.nfg.secs.core.ISystem;
	
	public class RenderingSystem implements ISystem 
	{
		var _renderableList;
		var _sprite;
		var visible;
		var position;
		
		public function RenderingSystem(sprite:Sprite, renderableList:EntityList) 
		{
			this._renderableList = renderableList;
			this._sprite = sprite;
		}
		
		public function update(ms:uint):void 
		{
			var i:int;
			for (i = 0; i < this._renderableList.length; i++) 
			{
				visible = this._renderableList.list[i].getComponent(Visible);
				position = this._renderableList.list[i].getComponent(Position);
			}
		}
	}
	
}
