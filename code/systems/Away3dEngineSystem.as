﻿package systems
{
	import flash.events.*;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	
	import away3d.cameras.Camera3D;
	import away3d.containers.Scene3D;
	import away3d.containers.View3D;
	import away3d.controllers.HoverController;
	import away3d.debug.AwayStats;
	import away3d.materials.lightpickers.StaticLightPicker;
	import MyAway3dClasses.MaterialsController;
	import away3d.entities.*;
	import away3d.core.base.Geometry;
	import away3d.primitives.*;
	import away3d.lights.DirectionalLight;
	import flash.geom.Vector3D;
	import away3d.materials.*;
	
	
	import io.nfg.secs.EntityList;
	import components.*;
	import io.nfg.secs.core.ISystem;
	import MyAway3dClasses.Away3dScreenArea;
	
	//import away3d.events.*;
	
	public class Away3dEngineSystem implements ISystem 
	{
		var MainScene:Sprite;
		// Away3D4 Vars
		public var scene:Scene3D;
		private var camera:Camera3D;
		public var view:View3D;
		private var cameraController:HoverController;
		private var lightPicker:StaticLightPicker;
		private var stats:AwayStats;
		private var directionalLight:DirectionalLight;
		private var CameraAngleY:int;
		public var ScreenArea:Away3dScreenArea = new Away3dScreenArea();
		
		// Away3D Config
		private var antiAlias:Number = 2;
		var statEnabled:Boolean;
		public var MaterialsHandler:MaterialsController = new MaterialsController(lightPicker);
		public var Geometry;
		public var Entities;
		

		public function Away3dEngineSystem(_MainScene:Sprite,_GameEntityList,_statEnabled:Boolean = false) 
		{
			MainScene = _MainScene;
			statEnabled = _statEnabled;
			Entities =_GameEntityList;
			setupAway3D4();
			setupLights();
			//TestStuff();
			
			MainScene.stage.scaleMode = StageScaleMode.NO_SCALE;
			MainScene.stage.align = StageAlign.TOP_LEFT;
			// Setup resize handler
			MainScene.stage.addEventListener(Event.RESIZE, resizeHandler);
		}

		private function setupAway3D4():void
		{
			scene = new Scene3D();
			camera = new Camera3D();
			view = new View3D();
			view.scene = scene;
			view.camera = camera;
			view.camera.y = 10000;
			view.camera.lens.far = 100000;
			view.camera.lookAt(new Vector3D(0,0,0));
			CameraAngleY = 90;
			view.camera.rotationX = CameraAngleY;
			view.antiAlias = antiAlias;
			MainScene.addChild(view);
			// Show Away3D stats
			if(statEnabled)
			{
				stats = new AwayStats(view,true);
				MainScene.addChild(stats);
			}
			trace("fullScreenWidth " + MainScene.stage.fullScreenWidth);
			trace("fullScreenHeight " + MainScene.stage.fullScreenHeight);
			ScreenArea.setProperties(view.width,view.height,camera);
			
		}
		private function TestStuff():void
		{
			///tests
			//MainScene.stage.addEventListener(MouseEvent3D.CLICK,Clicked);
			//MainScene.stage.mouseEnabled = true;
			
		}
		private function setupLights():void
		{
			directionalLight =  new DirectionalLight(-1, -1, 1);
			directionalLight.specular = 0.3;
			scene.addChild(directionalLight);
			lightPicker = new StaticLightPicker([directionalLight]);
		}
		private function setupGeometry():void
		{
			Geometry = new Mesh( new CubeGeometry(3000,3000,3000));
		}
		public function update(ms:uint):void 
		{
			updatePositions();
			view.render();
		}
		var CurrentMeshObject;
		var CurrentObject;
		private function updatePositions():void
		{
			for(var b:int = 0;b < Entities.length;b++)
				{
					CurrentMeshObject = Entities.list[b].getComponent(MeshObject).mesh;
					CurrentObject = Entities.list[b].getComponent(Position);
					CurrentMeshObject.x = CurrentObject.x;
					CurrentMeshObject.z = CurrentObject.z;
					CurrentMeshObject.y = CurrentObject.y;
				}
		}
		private function resizeHandler(e:Event=null):void
		{
			trace("resize handler actived");
			// Position Away3D4s view
			view.width = MainScene.stage.stageWidth;
			view.height = MainScene.stage.stageHeight;
			
			ScreenArea.setProperties(view.width,view.height,camera);
		}
		
		
		/*private function Clicked(event:MouseEvent3D):void
		{
			trace("|Clicked| z = " + event.scenePosition.z + "x = " + event.scenePosition.x);
		}*/
		
		public function addObject():void
		{
			trace("IT CALLED FUCK YEAH");
		}
		
	}
	
}
