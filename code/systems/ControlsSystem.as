﻿package systems
{
	import io.nfg.secs.core.ISystem;
	import io.nfg.secs.EntityList;
	import components.*;
	
	public class ControlsSystem implements ISystem 
	{
		var ObjectsToControlList:EntityList;
		var ControlsRef;
		var CurrentObject;
		var LevelLimits;

		public function ControlsSystem(Controls,_ObjectsToControlList:EntityList,_LevelLimits) 
		{
			ControlsRef = Controls;
			ObjectsToControlList = _ObjectsToControlList;
			
			LevelLimits = _LevelLimits;
		}
		
		public function update(ms:uint):void 
		{
			if(ControlsRef.CurrentButtonPressed != "None")
			{
				for(var b:int = 0;b < ObjectsToControlList.length;b++)
				{
					CurrentObject = ObjectsToControlList.list[b].getComponent(Position);
					if(ControlsRef.CurrentButtonPressed == "R" && CurrentObject.x < LevelLimits.right)
					{
						CurrentObject.x +=100;
					}
					else if(ControlsRef.CurrentButtonPressed == "L" && CurrentObject.x > LevelLimits.left)
					{
						CurrentObject.x -=100
					} 
				}
			}
		}

	}
	
}
