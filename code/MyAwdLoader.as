﻿package  
{
	
	import flash.display.Sprite;
	import flash.events.*;
	
	import away3d.animators.*;
	import away3d.animators.data.*;
	import away3d.animators.nodes.*;
	import away3d.animators.transitions.*;
	import away3d.containers.*;
	import away3d.entities.*;
	import away3d.events.*;
	import away3d.library.*;
	import away3d.library.assets.*;
	import away3d.loaders.*;
	import away3d.loaders.parsers.*;
	import away3d.materials.*;
	import away3d.core.base.Geometry;
	import away3d.core.base.SubMesh;
	import away3d.core.base.SubGeometry;
	
	
	public class MyAwdLoader extends Sprite
	{
		[Embed(source="../assets/3dmodels/play.awd",mimeType="application/octet-stream")]
		public static var PlayButton:Class;
		[Embed(source="../assets/3dmodels/lvl.awd",mimeType="application/octet-stream")]
		public static var LvlButton:Class;

		[Embed(source="../assets/3dmodels/choose.awd",mimeType="application/octet-stream")]
		public static var choose:Class;
		[Embed(source="../assets/3dmodels/back.awd",mimeType="application/octet-stream")]
		public static var back:Class;
		[Embed(source="../assets/3dmodels/letter_1.awd",mimeType="application/octet-stream")]
		public static var letter_1:Class;
		[Embed(source="../assets/3dmodels/letter_2.awd",mimeType="application/octet-stream")]
		public static var letter_2:Class;
		
		//animation constants
		public var animator:SkeletonAnimator;
        private var animationSet:SkeletonAnimationSet;
		//loader
		var assetsThatAreloaded:int = 0;//сколько загруженно 
		var assetsToLoad:int; //сколько загрузить
		var rootPose:SkeletonPose = new SkeletonPose();
		
		
		public function MyAwdLoader() 
		{
			
			AssetLibrary.enableParser(AWD2Parser);
		   AssetLibrary.addEventListener(AssetEvent.ASSET_COMPLETE, onAssetComplete);
		   AssetLibrary.addEventListener(LoaderEvent.RESOURCE_COMPLETE, onResourceComplete);
		   AssetLibrary.addEventListener(LoaderEvent.LOAD_ERROR, onModelLoadError);
			
			
			AssetLibrary.loadData(new PlayButton());
			AssetLibrary.loadData(new LvlButton());
			AssetLibrary.loadData(new choose());
			AssetLibrary.loadData(new back());
			AssetLibrary.loadData(new letter_2());
			AssetLibrary.loadData(new letter_1());
			assetsToLoad = 6;//сколько раз вызываем loaddata столько и файлов
		}
		
		 private function onAssetComplete(event:AssetEvent):void
		{
			if (event.asset.assetType == AssetType.SKELETON) 
			{
				trace(" AssetType.SKELETON");
                //create a new skeleton animation set
                animationSet = new SkeletonAnimationSet(3);
                //wrap our skeleton animation set in an animator object and add our sequence objects
                animator = new SkeletonAnimator(animationSet, event.asset as Skeleton, true);
            }
			else if (event.asset.assetType == AssetType.ANIMATION_NODE) 
			{
				
                //add each animation node to the animation set
                var animationNode:SkeletonClipNode = event.asset as SkeletonClipNode;
                animationSet.addAnimation(animationNode);
				
				trace("AssetType.ANIMATION_NODE name " + animationNode.name);
            }
        }
        
        /**
         * Check if all resource loaded
         */
        private function onResourceComplete(e:LoaderEvent):void
		{
			assetsThatAreloaded++;
			if (assetsThatAreloaded == assetsToLoad) 
			{
				AssetLibrary.removeEventListener(AssetEvent.ASSET_COMPLETE, onAssetComplete);
				AssetLibrary.removeEventListener(LoaderEvent.RESOURCE_COMPLETE, onResourceComplete);
				AssetLibrary.removeEventListener(LoaderEvent.LOAD_ERROR, onModelLoadError);
				this.dispatchEvent(new Event("awdLoaded",true,true));
			}
        }

		private function onModelLoadError(e:LoaderEvent):void
		{
			trace("error "+ e.url);
		}
  }
}
