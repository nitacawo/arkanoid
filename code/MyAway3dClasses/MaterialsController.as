﻿package MyAway3dClasses
{
	import away3d.materials.lightpickers.StaticLightPicker;
	import away3d.materials.*;
	import away3d.materials.methods.*;
	
	public class MaterialsController 
	{
		public var MainMaterial;
		public var DisabledMaterial;
		var Lightpicker;

		public function MaterialsController(_lightPicker:StaticLightPicker) 
		{
			Lightpicker = _lightPicker;
			initMats();
		}
		private function initMats():void
		{
			MainMaterial = new ColorMaterial(0xe0e05a);
			MainMaterial.diffuseMethod = new CelDiffuseMethod(6);
			MainMaterial.specularMethod = new CelSpecularMethod();
			MainMaterial.lightPicker = Lightpicker;
			
			//material for disabled level select
			DisabledMaterial = new ColorMaterial(0xffffa9);
			DisabledMaterial.diffuseMethod = new CelDiffuseMethod(6);
			DisabledMaterial.specularMethod = new CelSpecularMethod();
			DisabledMaterial.lightPicker = Lightpicker;
			
			
		}
		

	}
	
}
