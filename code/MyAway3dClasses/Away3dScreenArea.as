﻿package MyAway3dClasses
{
	
	public class Away3dScreenArea 
	{
		public var LevelLimits:LevelLimitsClass = new LevelLimitsClass ();
		
		var CameraAngleY:Number = 60;

		public function Away3dScreenArea() 
		{
			
		}
		public function setProperties(ScreenWidth,ScreenHeight,camera)
		{
			var distFromCamToPlane:Number = Math.abs(camera.y) + 0;
			var planeHeight = Math.tan((Math.PI / 180) * (CameraAngleY * 0.5)) * distFromCamToPlane;
			var aspectRatio:Number = ScreenWidth / ScreenHeight;
			var planeWidth = planeHeight * aspectRatio;
			
			trace("planeWidth " + planeWidth);
			trace("planeHeight " + planeHeight );
			
			LevelLimits.left = -planeWidth;
			LevelLimits.right = planeWidth;
			LevelLimits.top = -planeHeight;
			LevelLimits.bottom = planeHeight;
			
			
		}
		

	}
	
}
