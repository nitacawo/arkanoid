﻿package  
{
	import away3d.core.base.Geometry;
	import away3d.primitives.*;
	import away3d.entities.*;
	
	public class Away3dObjectsHandler 
	{
		var ArrayOfObjects:Array = new Array();
		var ArrayOfLocations:Array = new Array();
		var Geometry;

		public function Away3dObjectsHandler() 
		{
			//generate all neccessary objects
			button();
		}
		private function button():void
		{
			Geometry = new Mesh( new CubeGeometry(500,500,500));
			ArrayOfObjects.push(Geometry);
		}
		
		var ChildToReturn:Mesh;
		public function GiveMeObject(WhatObject:String):Mesh 
		{
			switch (WhatObject)
			{
				case "Button" :
					ChildToReturn = Mesh(ArrayOfObjects[0].clone());
					//ChildToReturn = ArrayOfObjects[0];
				break;
			}
			return ChildToReturn;
		}

	}
	
}
