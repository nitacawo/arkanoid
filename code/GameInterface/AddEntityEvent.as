﻿package GameInterface  
{
	import flash.events.Event;
	import flash.geom.Vector3D;
	
	public class AddEntityEvent extends Event
	{
		public static const ADD:String = "Add";
		
		public var name:String;
		public var coordinates:Vector3D;

		public function AddEntityEvent(type:String, name:String,coordinates:Vector3D = null, bubbles:Boolean=false, cancelable:Boolean=false) 
		{
			super(type, bubbles, cancelable);
			this.name = name;
			this.coordinates = coordinates;
			trace("coordinates " + coordinates);
		}
		public override function clone():Event
		{
			return new AddEntityEvent(type, name,coordinates, bubbles, cancelable);
		}

	}
	
}
