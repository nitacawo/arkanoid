﻿package GameInterface  
{
	import flash.events.Event;
	import away3d.entities.*;

	
	public class EventsControllerEvent extends Event
	{
		public static const NAME:String = "Name";
		
		public var name:String;
		public var mesh:Mesh;

		public function EventsControllerEvent(type:String, name:String,mesh:Mesh = null, bubbles:Boolean=false, cancelable:Boolean=false) 
		{
			super(type, bubbles, cancelable);
			this.name = name;
			this.mesh = mesh;
		}
		public override function clone():Event
		{
			return new EventsControllerEvent(type, name,mesh, bubbles, cancelable);
		}

	}
	
}
