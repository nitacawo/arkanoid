﻿package GameInterface 
{
	import flash.events.*;
	import GameInterface.EventsControllerEvent;
	import flash.display.Sprite;
	import flash.geom.Vector3D;
	import io.nfg.secs.EntityList;
	import components.*;
	
	public class EventsControllerClass extends Sprite
	{
		var MainScene;
		public var CurrentLevelSelected:int = 1;
		var RenderableEntityList;

		public function EventsControllerClass(_MainScene,RenderableEntityList) 
		{
			this.RenderableEntityList = RenderableEntityList;
			MainScene = _MainScene;
			MainScene.addEventListener(EventsControllerEvent.NAME, ClickHandler);
			
			
			//CreateMenu();
		}
		public function init():void
		{
			trace("wtf");
			this.dispatchEvent(new AddEntityEvent(AddEntityEvent.ADD, "play",new Vector3D(500,0,1000),true,true));
			this.dispatchEvent(new AddEntityEvent(AddEntityEvent.ADD, "lvl",new Vector3D(500,0,-2000),true,true));
		}
		private function ClickHandler(e:EventsControllerEvent):void
		{
			trace(e.name);
			
			//menu logic
			if(e.name == "play")
			{
				trace("play logic");
				this.dispatchEvent(new RemoveEntityEvent(RemoveEntityEvent.NAME, e.name));
			}
			else if(e.name == "lvl")
			{
				trace("lvl logic");
				this.dispatchEvent(new RemoveEntityEvent(RemoveEntityEvent.NAME, "play"));
				this.dispatchEvent(new RemoveEntityEvent(RemoveEntityEvent.NAME, "lvl"));
				
				this.dispatchEvent(new AddEntityEvent(AddEntityEvent.ADD, "levelChoice"));
			}
			else if(e.name == "letter_1")
			{
				CurrentLevelSelected = 1;
				e.mesh.scaleX = 40;
				e.mesh.scaleY = 40;
				e.mesh.scaleZ = 40;
				
				for(var counter:int = 0; counter < RenderableEntityList.length;counter++)
				{
					if(RenderableEntityList.list[counter].getComponent(MeshObject).name == "letter_2")
					{
						RenderableEntityList.list[counter].getComponent(MeshObject).mesh.scaleX = 20;
						RenderableEntityList.list[counter].getComponent(MeshObject).mesh.scaleY = 20;
						RenderableEntityList.list[counter].getComponent(MeshObject).mesh.scaleZ = 20;
					}
				}
			}
			else if(e.name == "letter_2")
			{
				CurrentLevelSelected = 2;
				
				e.mesh.scaleX = 40;
				e.mesh.scaleY = 40;
				e.mesh.scaleZ = 40;
				
				for(var counter:int = 0; counter < RenderableEntityList.length;counter++)
				{
					if(RenderableEntityList.list[counter].getComponent(MeshObject).name == "letter_1")
					{
						RenderableEntityList.list[counter].getComponent(MeshObject).mesh.scaleX = 20;
						RenderableEntityList.list[counter].getComponent(MeshObject).mesh.scaleY = 20;
						RenderableEntityList.list[counter].getComponent(MeshObject).mesh.scaleZ = 20;
					}
				}
			}
			else if(e.name == "back")
			{
				trace("back logic");
				this.dispatchEvent(new RemoveEntityEvent(RemoveEntityEvent.NAME, "back"));
				this.dispatchEvent(new RemoveEntityEvent(RemoveEntityEvent.NAME, "choose"));
				this.dispatchEvent(new RemoveEntityEvent(RemoveEntityEvent.NAME, "letter_1"));
				this.dispatchEvent(new RemoveEntityEvent(RemoveEntityEvent.NAME, "letter_2"));
				
				
				this.dispatchEvent(new AddEntityEvent(AddEntityEvent.ADD, "play",new Vector3D(500,0,1000),true,true));
				this.dispatchEvent(new AddEntityEvent(AddEntityEvent.ADD, "lvl",new Vector3D(500,0,-2000),true,true));
			}
		}
	}
}

