﻿package GameInterface  
{
	import flash.events.Event;
	import io.nfg.secs.Entity;
	
	public class RemoveEntityEvent extends Event
	{
		public static const NAME:String = "Name";
		
		public var name:String;
		public var entity:Entity;

		public function RemoveEntityEvent(type:String, name:String,entity:Entity = null, bubbles:Boolean=false, cancelable:Boolean=false) 
		{
			super(type, bubbles, cancelable);
			this.name = name;
			this.entity = entity;
		}
		public override function clone():Event
		{
			return new RemoveEntityEvent(type, name,entity, bubbles, cancelable);
		}

	}
	
}
