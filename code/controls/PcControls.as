﻿package controls
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.ui.Keyboard;
	import flash.events.KeyboardEvent;
	
	import away3d.events.*;
	
	public class PcControls
	{
		var MainScene:Sprite;
		public var CurrentButtonPressed:String = "None";

		public function PcControls(_MainScene:Sprite) 
		{
			MainScene = _MainScene;
			MainScene.stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
			MainScene.stage.addEventListener(KeyboardEvent.KEY_UP, keyUpHandler);
			
			//away3d mouse click
			//MainScene.addEventListener(MouseEvent3D.CLICK,Clicked);
		}
		
		function keyDownHandler(e:KeyboardEvent):void
		{
			if(e.keyCode == Keyboard.LEFT || e.keyCode==65) CurrentButtonPressed = "L";
			else if(e.keyCode == Keyboard.RIGHT || e.keyCode==68) CurrentButtonPressed = "R";
			//trace("buttons pressed" + CurrentButtonPressed);
		}
		function keyUpHandler(e:KeyboardEvent):void
		{
			if((e.keyCode == Keyboard.LEFT || e.keyCode==65) && CurrentButtonPressed == "L")CurrentButtonPressed = "None";
			else if((e.keyCode == Keyboard.RIGHT || e.keyCode==68) && CurrentButtonPressed == "R")CurrentButtonPressed = "None";
		}
		/*
		private function Clicked(event:MouseEvent3D):void
		{
			trace("|Clicked| z = " + event.scenePosition.z + "x = " + event.scenePosition.x );
		}
		*/

	}
	
}
