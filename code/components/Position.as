﻿package components
{
	import io.nfg.secs.core.IComponent;
	
	public class Position implements IComponent
	{
		public var x:Number = 0;
		public var y:Number = 0;
		public var z:Number = 0;
		
		public function Position()
		{
			
		}

		public function setXY(x:Number, y:Number, z:Number):Position 
		{
			this.x = x;
			this.y = y;
			this.z = z;
			return this;
		}
	}
	
}
