﻿package components
{
	import io.nfg.secs.core.IComponent;
	
	import away3d.events.*;
	import flash.events.*;
	import GameInterface.EventsControllerEvent;
	import away3d.entities.*;
	
	public class AwayClickable  implements IComponent
	{
		var MainScene;
		var Name:String;
		var MeshRef:Mesh;

		public function AwayClickable() 
		{
			
		}
		public function Activate(MeshRef:Mesh,_MainScene,_Name):AwayClickable
		{
			this.MeshRef = MeshRef;
			MeshRef.addEventListener(MouseEvent3D.CLICK,Clicked);
			MeshRef.mouseEnabled = true;
			MainScene = _MainScene;
			Name = _Name;
			return this;
		}
		
		
		private function Clicked(event:MouseEvent3D):void
		{
			trace("|Clicked| z = " + event.scenePosition.z + "x = " + event.scenePosition.x);
			MainScene.dispatchEvent(new EventsControllerEvent(EventsControllerEvent.NAME, Name,MeshRef));
		}
		public function remove():void
		{
			MeshRef.removeEventListener(MouseEvent3D.CLICK,Clicked);
		}

	}
	
}
