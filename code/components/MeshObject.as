﻿package components
{
	import away3d.entities.*;
	import away3d.materials.*;
	import io.nfg.secs.core.IComponent;
	
	public class MeshObject  implements IComponent
	{
		public var mesh:Mesh;
		public var material;
		public var name:String;
		var AwayRef;

		public function MeshObject() 
		{
			
		}
		public function setObject(thismesh,AwayRef,name:String):MeshObject
		{
			this.mesh = thismesh;
			this.name = name;
			this.AwayRef = AwayRef;
			//this.mesh = Mesh(AwayRef.Geometry.clone());
			this.mesh.material = AwayRef.MaterialsHandler.MainMaterial;
			AwayRef.scene.addChild(mesh);
			return this;
		}
		public function remove()
		{
			if(this.mesh != undefined)
			{
				
				AwayRef.scene.removeChild(mesh);
			}
			
		}
	}
	
}