﻿package components 
{
	import away3d.entities.*;
	import away3d.materials.*;
	import io.nfg.secs.core.IComponent;
	
	public class Visible implements IComponent
	{
		public var mesh:Mesh;
		public var material:TextureMaterial;

		public function Visible() 
		{
			// constructor code
		}

	}
	
}
