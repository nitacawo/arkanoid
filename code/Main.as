﻿package  
{
	
	import flash.display.Sprite;
	import flash.events.*;
	
	
	import controls.*;
	import MyAway3dClasses.*;
	import GameInterface.*;

	public class Main extends Sprite
	{
		
		var EControls:EnviromentControls;
		var SECS:SECSHandler;
		var Interface:InterfaceHandler;
		var AwdLoader:MyAwdLoader = new MyAwdLoader();
		var ModelsLoaded:Boolean = false;
		var AddedTostage:Boolean = false;
		public function Main() 
		{
			AwdLoader.addEventListener("awdLoaded", awdready);
			this.addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler, false, 0, true);
			
			this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		private function onEnterFrame(e:Event):void
		{
			if(ModelsLoaded && AddedTostage)
			{
				this.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
				
				SECS.Away3dMode();
			}
			
		}
		private function awdready(e:Event):void
		{
			AwdLoader.removeEventListener("awdLoaded", awdready);
			trace("awdLoaded");
			ModelsLoaded = true;	
		}
		private function addedToStageHandler(e:Event):void
		{
			AddedTostage = true;
			this.removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
			//Activating systems
			EControls = new EnviromentControls(this,false);
			SECS = new SECSHandler(this,EControls.Controls);
			Interface = new InterfaceHandler(this);
			
		}
	}
}
