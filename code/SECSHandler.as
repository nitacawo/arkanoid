﻿package  
{
		
	import io.nfg.secs.Entity;
	import io.nfg.secs.Engine;
	import io.nfg.secs.EntityList;
	import flash.display.Sprite;
	
	import components.*;
	import systems.*;
	import MyAway3dClasses.*;
	import GameInterface.EventsControllerClass;
	import GameInterface.RemoveEntityEvent;
	import GameInterface.AddEntityEvent;
	import flash.events.*;
	import flash.geom.Vector3D;
	
	import away3d.library.*;

	public class SECSHandler 
	{
		var entity;
		var MainScene:Sprite;
		var Controls;
		var EventsController:EventsControllerClass;
		
		// -> entity lists
		//var InterfaceEntityList:EntityList = Engine.getList([Position, MeshObject]);
		//var GameEntityList:EntityList = Engine.getList([Position, MeshObject]);
		
		

		var RenderableEntityList:EntityList = Engine.getList([Position, MeshObject]);
		public function SECSHandler(_MainScene:Sprite,_Controls) 
		{
			MainScene = _MainScene;
			Controls = _Controls;
			EventsController = new EventsControllerClass(MainScene,RenderableEntityList);
			EventsController.addEventListener(RemoveEntityEvent.NAME, RemoveRenderableEntity);// if I need to remove renderable object
			EventsController.addEventListener(AddEntityEvent.ADD, PushAsset);//if I need to add renderable object
			MainScene.addEventListener(Event.ENTER_FRAME, tick);
		}
		
		var AwayRef;
		var MeshContainer:Away3dObjectsHandler;
		
		//activated only after awd models been loaded
		public function Away3dMode():void
		{
			Engine.addSystem(new Away3dEngineSystem(MainScene,RenderableEntityList,true));
			//reference to away3d engine as it's quite frustrating to update objects in the engine
			
			
			MeshContainer = new Away3dObjectsHandler();
			
			AwayRef = Engine.getSystem(Away3dEngineSystem) as Away3dEngineSystem;
			//objects to respond on keyboard and other inputs
			Engine.addSystem(new ControlsSystem(Controls,RenderableEntityList,AwayRef.ScreenArea.LevelLimits));
			
			EventsController.init();
		}
		var CurrentObject;
		public function PushEntity(WhatToPush:String,Location:Vector3D = null,Name:String = null):void
		{
			switch (WhatToPush)
			{
				case "Button" :
					CurrentObject = MeshContainer.GiveMeObject("Button");
					entity = new Entity();
					entity.addComponentWith(new Position().setXY(Location.x, Location.y,Location.z)).addComponentWith(new MeshObject().setObject(CurrentObject,AwayRef,WhatToPush));
					entity.addComponentWith(new AwayClickable().Activate(CurrentObject,MainScene,Name));
					Engine.addEntity(entity);
				break;
			}
			trace("RenderableEntityList.length " + RenderableEntityList.length);
		}
		function PushAsset(e:AddEntityEvent):void
		//function PushAsset(WhatToPush:String,Location:Vector3D = null):void
		{
			trace("e.name " + e.name);
			if(e.name == "levelChoice")
			{
				trace("levelChoice objects");
				CurrentObject = AssetLibrary.getAsset("choose");
				CurrentObject.scaleX = 10;
				CurrentObject.scaleY = 10;
				CurrentObject.scaleZ = 10;
				entity = new Entity();
				var VectorToPlace = e.coordinates;
				entity.addComponentWith(new Position().setXY(500,-1000,2000)).addComponentWith(new MeshObject().setObject(CurrentObject,AwayRef,"choose"));
				entity.addComponentWith(new AwayClickable().Activate(CurrentObject,MainScene,"choose"));
				Engine.addEntity(entity);
				
				
				CurrentObject = AssetLibrary.getAsset("back");
				CurrentObject.scaleX = 10;
				CurrentObject.scaleY = 10;
				CurrentObject.scaleZ = 10;
				entity = new Entity();
				var VectorToPlace = e.coordinates;
				entity.addComponentWith(new Position().setXY(500,0,-4000)).addComponentWith(new MeshObject().setObject(CurrentObject,AwayRef,"back"));
				entity.addComponentWith(new AwayClickable().Activate(CurrentObject,MainScene,"back"));
				Engine.addEntity(entity);
				
				CurrentObject = AssetLibrary.getAsset("letter_1");
				if( EventsController.CurrentLevelSelected == 1)
				{
					CurrentObject.scaleX = 40;
					CurrentObject.scaleY = 40;
					CurrentObject.scaleZ = 40;
				}
				else
				{
					CurrentObject.scaleX = 20;
					CurrentObject.scaleY = 20;
					CurrentObject.scaleZ = 20;
				}
				entity = new Entity();
				var VectorToPlace = e.coordinates;
				entity.addComponentWith(new Position().setXY(-2000,0,-2000)).addComponentWith(new MeshObject().setObject(CurrentObject,AwayRef,"letter_1"));
				entity.addComponentWith(new AwayClickable().Activate(CurrentObject,MainScene,"letter_1"));
				Engine.addEntity(entity);
				
				CurrentObject = AssetLibrary.getAsset("letter_2");
				if( EventsController.CurrentLevelSelected == 2)
				{
					CurrentObject.scaleX = 40;
					CurrentObject.scaleY = 40;
					CurrentObject.scaleZ = 40;
				}
				else
				{
					CurrentObject.scaleX = 20;
					CurrentObject.scaleY = 20;
					CurrentObject.scaleZ = 20;
				}
				entity = new Entity();
				var VectorToPlace = e.coordinates;
				entity.addComponentWith(new Position().setXY(1000,0,-2000)).addComponentWith(new MeshObject().setObject(CurrentObject,AwayRef,"letter_2"));
				entity.addComponentWith(new AwayClickable().Activate(CurrentObject,MainScene,"letter_2"));
				Engine.addEntity(entity);
			}
			else
			{
				CurrentObject = AssetLibrary.getAsset(e.name);
				CurrentObject.scaleX = 20;
				CurrentObject.scaleY = 20;
				CurrentObject.scaleZ = 20;
				entity = new Entity();
				var VectorToPlace = e.coordinates;
				entity.addComponentWith(new Position().setXY(VectorToPlace.x, VectorToPlace.y,VectorToPlace.z)).addComponentWith(new MeshObject().setObject(CurrentObject,AwayRef,e.name));
				entity.addComponentWith(new AwayClickable().Activate(CurrentObject,MainScene,e.name));
				Engine.addEntity(entity);
			}
		}
		function RemoveRenderableEntity(e:RemoveEntityEvent):void
		{
			trace("RenderableEntityList.length " + RenderableEntityList.length);
			for(var counter:int = 0; counter < RenderableEntityList.length;counter++)
			{
				if(RenderableEntityList.list[counter].getComponent(MeshObject).name == e.name)
				{
					RenderableEntityList.list[counter].getComponent(MeshObject).remove();
					RenderableEntityList.list[counter].getComponent(AwayClickable).remove();
					Engine.removeEntity(RenderableEntityList.list[counter]);
				}
			}
		}
		public function tick(e:Event):void 
		{
			Engine.update(60);
		}
	}
	
}
